# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# tasks/packages.yml
host_specific_custom_packages__to_merge:
  bc: "latest"
  gimp: "absent"
  joe: "present"

# tasks/groups.yml
host_specific_custom_groups__to_merge:
  example_group:
    state: "present"
    gid: "1234"
    system: "false"

# tasks/users.yml
host_specific_custom_users__to_merge:
  example:
    comment: "Host-specific example user (via Ansible)"
    groups:
      - "users"
      - "staff"
      - "example_group"
    password: "$6$bCrd2ylJ$uQ.uN7Oy1QStmunslSMzYX0vM4wYXKdO9tgu.SE.hPdKBqcHk.CrHsUMxzN8rwiPcwRI3nzvzd9W4T0sJJmNz."
    authorized_keys: |
      ssh-rsa This is just an example authorized key
  absentExample:
    comment: "Absent example user (via Ansible)"
    authorized_keys: "nokey"
    state: "absent"

# tasks/services.yml
host_specific_custom_services__to_merge:
  example_disabled_service:
    enabled: false
    state: "stopped"
    service:
      Unit:
        Description: "Example disabled service"
      Service:
        Type: "oneshot"
        ExecStart: "/bin/false"
      Install:
        WantedBy: "multi-user.target"
  example_enabled_service:
    enabled: true
    service:
      Unit:
        Description: "Example enabled service"
        After: "network.target"
      Service:
        Type: "notify"
        ExecStart: "/bin/true"
        TimeoutStartSec: "3600"
        Restart: "always"
        RestartSec: "10"
        WorkingDirectory: "/var/tmp"
        User: "nobody"
        Group: "nogroup"
        LimitNOFILE: "49152"
      Install:
        WantedBy: "multi-user.target"

# provision-custom-systems.yml
host_specific_sshd_config__to_merge:
  AllowAgentForwarding: "no"
  AllowTcpForwarding: "no"

# tasks/ssh-keys.yml
host_specific_custom_ssh_keys__to_merge:
  example_key:
    path: "/root/test-ssh-key-example_id_ed25519"
    type: "ed25519"
    size: 521

# tasks/authorized-keys.yml
host_specific_custom_authorized_keys__to_merge:
  example_authorized_key:
    user: "root"
    key: "ssh-ed25519 \
      AAAAC3NzaC1lZDI1NTE5AAAAILob83aPj9IAcM4Tir1RFYBuDcepJrpP3PlfOv4qQiMC"
    manage_dir: "yes"
    comment:
      "test comment with spaces"
    key_options:
      - "command=\"/bin/true\""
      - "no-agent-forwarding"
      - "no-port-forwarding"
      - "no-pty"
      - "no-X11-forwarding"

# tasks/cron-jobs.yml
host_specific_custom_cron_jobs__to_merge:
  example_cron_job:
    name: "example_job"
    job: "/bin/true"
    user: "root"
    state: "present"

# https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
vagrant_box:
  "debian/stretch64"

# https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html
vagrant_groups:
  - "examples.test"
  - "unattended-upgrades"

# https://www.vagrantup.com/intro/getting-started/networking.html
vagrant_networks:
  - private_network:
      ip: "10.8.10.10"

# https://www.vagrantup.com/docs/provisioning/ansible.html
vagrant_playbooks:
  - "provision-custom-systems.yml"
  - "provision-unattended-upgrades.yml"
  - "provision-ssh-keys.yml"
  - "provision-authorized-keys.yml"
  - "provision-cron-jobs.yml"
