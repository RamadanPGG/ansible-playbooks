# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# https://docs.gitlab.com/ee/ci/yaml/#stages
stages:
  - "test"
  - "vagrant"

# Use `gitlab-runner exec docker rubocop` to execute in development
rubocop:
  image:
    "registry.gitlab.com/eyeo/docker/rubocop:0.63.0"
  script:
    - "set -- Vagrantfile"
    - "rubocop $@"
  stage:
    "test"

# Use `gitlab-runner exec docker yamllint` to execute in development
yamllint:
  image:
    "registry.gitlab.com/eyeo/docker/yamllint:1.14.0"
  script:
    - "set -- *.yml"
    - "set -- $@ `find group_vars/ host_vars/ -type f`"
    - "set -- $@ `find tasks/ -type f`"
    - "set -- $@ .gitlab-ci.yml"
    - "set -- $@ .vagrant-jobs.gitlab-ci.yml"
    - "set -- $@ .rubocop.yml"
    - "set -- $@ .yamllint.yml"
    - "yamllint -c .yamllint.yml $@"
  stage:
    "test"

include:
  local: ".vagrant-jobs.gitlab-ci.yml"

adblockplus-donate-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "adblockplus-donate-server-0.test"

adblockplus-publish-host-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "adblockplus-publish-host-0.test"

admin-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "admin-0.test"
    VAGRANT_BOX_NEEDS_CHECKED_OUT_BRANCH: "admin-0.test"

apache2-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "apache2-server-0.test"

apache2-mimeo-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "apache2-mimeo-server-0.test"

apache2-modsecurity-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "apache2-modsecurity-server-0.test"

apk-signing-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "apk-signing-0.test"

backup-client-0.test:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "backup-client-0.test"
  # requires persistent backup-server-0.test to complete
  allow_failure: true

backup-server-0.test:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "backup-server-0.test"

chromium-gitlab-runner-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "chromium-gitlab-runner-0.test"
  # https://gitlab.com/eyeo/devops/ansible-playbooks/issues/25
  allow_failure: true

custom-centos7-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "custom-centos7-0.test"

custom-debian9-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "custom-debian9-0.test"

custom-paths-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "custom-paths-0.test"

docker-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "docker-0.test"

docker-gitlab-runner-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "docker-gitlab-runner-0.test"

eramba-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "eramba-server-0.test"
  # requires proprietary binary blob to complete
  allow_failure: true

eyeo-cms-web-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "eyeo-cms-web-server-0.test"

gitlab-runner-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "gitlab-runner-0.test"

icinga-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "icinga-0.test"

lamp-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "lamp-server-0.test"

# libvirt-host-0 intentionally skipped as it can't have a vagrant/libvirt job

mattermost-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "mattermost-server-0.test"

munin-node-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "munin-node-0.test"

mysql-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "mysql-0.test"

mumble-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "mumble-0.test"

postfix-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "postfix-0.test"

postgresql-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "postgresql-0.test"

public-git-web-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "public-git-web-server-0.test"

telemetry-pingv1-server-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "telemetry-pingv1-server-0.test"

tmate-0:
  extends: ".vagrantjob"
  variables:
    TEST_HOST: "tmate-0.test"

# vagrant-runner-0 intentionally skipped as it can't have a vagrant/libvirt job
